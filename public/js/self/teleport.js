AFRAME.registerComponent('touch-listener', 
{
  init: function () 
  {
    var targetEl = this.el;

    if(AFRAME.utils.device.isGearVR())
    {
      // alert("hello vr");
      document.querySelector('a-scene').addEventListener('trackpaddown', function () 
      {
        isTriggered = true;
        if(isPressed === true)
        {
          menu();
        }
        targetEl.emit('starttouch')
      }) 
      document.querySelector('a-scene').addEventListener('trackpadup', function () 
      {
        isTriggered = false;
        targetEl.emit('endtouch')
      }) 
    }

    else if(AFRAME.utils.device.isMobile())
    {
      // alert("hello mobile");
      controller.setAttribute('visible','false');
      document.querySelector('a-scene').addEventListener('touchstart', function () 
      {
        targetEl.emit('starttouch');
      })

      document.querySelector('a-scene').addEventListener('touchend', function () 
      {
        targetEl.emit('endtouch');
      })
    }
    
    else
    {
      // alert("hello desktop"); 
      controller.setAttribute('visible','false');
      document.querySelector('a-scene').addEventListener('mousedown', function () 
      {
        targetEl.emit('starttouch')
      })

      document.body.addEventListener('keydown', function (e) 
      {
        if (e.keyCode == 32) 
        {
          console.log('space key pressed!');
          targetEl.emit('starttouch');      
        }  
      })
    }

    document.querySelector('a-scene').addEventListener('mouseup', function () 
    {
      targetEl.emit('endtouch')
    })

    document.body.addEventListener('keyup', function (e) 
    {
      if (e.keyCode == 32) 
      {
        console.log('space key released!');
        targetEl.emit('endtouch');      
      }  
    })
  }
});